<?php  
$site_url = 'http://'.$_SERVER['HTTP_HOST'];
//$site_url = $_SERVER['PHP_SELF'];
    session_start();
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
    $created_at = $_SESSION["created_at"];
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <?php
        if($role=='student')
        {
          echo '<img src="'.$site_url.'/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">';
        }
        else{
          echo '<p style="height:20px;;"></p>';
        }
        ?>  
      </div>
      <div class="pull-left info">
        <p><?php echo $role;?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo $site_url ?>/"><i class="fa fa-circle-o"></i> Home</a></li>
          <!-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li> -->
        </ul>
      </li>

      
      <?php /* ?>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Layout Options</span>
          <span class="pull-right-container">
            <span class="label label-primary pull-right">4</span>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
          <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
          <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
          <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
        </ul>
      </li>
      <li>
        <a href="pages/widgets.html">
          <i class="fa fa-th"></i> <span>Widgets</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green">new</small>
          </span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Charts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
          <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>UI Elements</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
          <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
          <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
          <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
          <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
          <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
        </ul>
      </li>
      <?php */ ?>


      <li class="treeview">
        <a href="">
          <i class="fa fa-edit"></i> <span>Forms</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <!-- <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
          <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
          <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li> -->

         <li><a href="<?php echo $site_url ?>/pages/forms/admin.php"><i class="fa fa-circle-o"></i> Admin Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/school.php"><i class="fa fa-circle-o"></i> School Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/affiliation.php"><i class="fa fa-circle-o"></i> Affiliation Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/branch.php"><i class="fa fa-circle-o"></i> Branch Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/evaluator.php"><i class="fa fa-circle-o"></i> Evaluator Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/report-card.php"><i class="fa fa-circle-o"></i> Report Card Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/subject.php"><i class="fa fa-circle-o"></i> Subject Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/exam.php"><i class="fa fa-circle-o"></i> Exam Form</a></li> 
         <li><a href="<?php echo $site_url ?>/pages/forms/student.php"><i class="fa fa-circle-o"></i> Student Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/teacher.php"><i class="fa fa-circle-o"></i> Teacher Form</a></li>
          <?php /* <li><a href="<?php echo $site_url ?>/pages/forms/subject-teacher.php"><i class="fa fa-circle-o"></i> Subject Teacher Form</a></li> */?>
          <li><a href="<?php echo $site_url ?>/pages/tables/class-detail.php"><i class="fa fa-circle-o"></i> Class Detail</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/standard.php"><i class="fa fa-circle-o"></i> Standard Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/principal.php"><i class="fa fa-circle-o"></i> Principal Form</a></li>

        </ul>
      </li>


     
      <li class="treeview">
        <a href="#">
          <i class="fa fa-table"></i> <span>Manage</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_school.php"><i class="fa fa-circle-o"></i> Manage School</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_branch.php"><i class="fa fa-circle-o"></i> Manage Branch</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_principal.php"><i class="fa fa-circle-o"></i> Manage Principal</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_teacher.php"><i class="fa fa-circle-o"></i> Manage Teacher</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/select_class.php"><i class="fa fa-circle-o"></i> Manage Student</a></li>
          <!--<li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Manage Student</a></li>-->
          <!--<li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>-->
        </ul>
      </li>
      <li class="treeview">
       <a href="#">
         <i class="fa fa-table"></i> <span>ReportCard</span>
         <span class="pull-right-container">
           <i class="fa fa-angle-left pull-right"></i>
         </span>
       </a>
       <ul class="treeview-menu">
           <li><a href="<?php echo $site_url ?>/pages/forms/scholastic-page.php"><i class="fa fa-circle-o"></i> Register Marks</a></li>
           <li><a href="<?php echo $site_url ?>/pages/forms/select_report_card.php"><i class="fa fa-circle-o"></i> View Report Card</a></li>
       </ul>
     </li>
    
     
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>