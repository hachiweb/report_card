<?php 
 session_start();
    if(empty($_SESSION["username"])){
        header("location:pages/auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('header.php'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>150</h3>

            <p>School</p>
          </div>
          <div class="icon">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/school.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>53<sup style="font-size: 20px">%</sup></h3>

            <p>Affiliation</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/affiliation.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>44</h3>

            <p>Branch</p>
          </div>
          <div class="icon">
            <i class="fa fa-snowflake-o" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/branch.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>65</h3>

            <p>Evaluator</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/evaluator.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->


      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3>65</h3>

            <p>Report Card</p>
          </div>
          <div class="icon">
            <i class="fa fa-file-text" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/report-card.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3>65</h3>

            <p>Report Card</p>
          </div>
          <div class="icon">
            <i class="fa fa-file-text" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/table-term.php" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>65</h3>

            <p>Subject</p>
          </div>
          <div class="icon">
            <i class="fa fa-clipboard" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/subject.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3>65</h3>

            <p>Teacher</p>
          </div>
          <div class="icon">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/teacher.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>65</h3>

            <p>Subject Teacher</p>
          </div>
          <div class="icon">
            <i class="fa fa-linode" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/subject-teacher.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>65</h3>

            <p>Standard</p>
          </div>
          <div class="icon">
            <i class="fa fa-pie-chart" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/standard.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
      
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
          <div class="inner">
            <h3>65</h3>

            <p>Student</p>
          </div>
          <div class="icon">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/student.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-orange">
          <div class="inner">
            <h3>65</h3>

            <p>Principal</p>
          </div>
          <div class="icon">
            <i class="fa fa-male" aria-hidden="true"></i>
          </div>
          <a href="<?php echo $site_url ?>/pages/forms/principal.php" class="small-box-footer">Register <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

    </div><!-- /.row -->
    <!-- Main row -->
  </section>
  <!-- /.content -->
</div>

<!-- /.content-wrapper -->
<?php include('footer.php'); ?>
