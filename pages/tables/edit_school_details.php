<?php
 session_start();
$site_url = 'https://'.$_SERVER['HTTP_HOST'];
//$site_url = $_SERVER['PHP_SELF'];
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    };
    
include('../../header.php');

?>    
    


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage School
        <small>edit records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">school</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">School Record</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form method="POST" action="edit_school_details_core.php" enctype="multipart/form-data">
                <table>
  
                <?php 

                require_once "../../dbconnect.php";

                $db = new DB();

                $id = $_GET['id'];
                $school_query = "SELECT * FROM school WHERE id = '$id'";

                $select_data = $db->executeQuery($school_query);

                $school_data = mysqli_fetch_array($select_data);

                ?>


                  <tr>
                  <td><label class="m-2" for="">Registration No.</label></td>
                  <td><input type="text" name="registration_no" value="<?php echo $school_data['registration_no']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">School Code</label></td>
                  <td><input type="text" name="school_code" value="<?php echo $school_data['school_code']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">School name</label></td>
                  <td><input type="text" name="school_name" value="<?php echo $school_data['school_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">school logo</label></td>
                  <td><input type="file" name="school_logo" value="<?php echo $school_data['school_logo']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Status</label></td>
                  <td><input type="text" name="status" value="<?php echo $school_data['status']; ?>"></td>
                </tr>
                <input type="hidden" value="<?php echo $school_data['id']; ?>" name="id">
                </table>

                <div><input class="ml-15 m-3" type="submit" value="Update"></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include('../../footer.php'); ?>