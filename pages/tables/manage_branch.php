<?php
 session_start();
$site_url = 'https://'.$_SERVER['HTTP_HOST'];
//$site_url = $_SERVER['PHP_SELF'];
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    };
    
include('../../header.php');

?>    
    


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Branch
        <small>View records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Branch</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Branch Record</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Serial No.</th>
                  <th>Branch Code</th>
                  <th>Branch Address</th>
                  <th>Branch City</th>
                  <th>Branch State</th>
                  <th>Brunch Pincode</th>
                  <th>Status</th>
                </tr>
                <?php 
                require_once "../../dbconnect.php";



                $branch_id = $_SESSION["branch_id"];

                $db = new DB();

                //extract from branch table
                $branch_query = "SELECT * FROM branch WHERE id='$branch_id'";
                $select_data = $db->executeQuery($branch_query);
                $n = 1;
                while ($branch_data = mysqli_fetch_array($select_data)) {
                ?>
                  <tr style="<?php if(isset($_GET['id'])){
                    if($_GET['id'] == $branch_data['id']){
                      echo 'background: #00FFFF';
                    };
                  }; ?>">
                    <td><?php echo $n++; ?></td>
                    <td><?php echo $branch_data['branch_code']; ?></td>
                    <td><?php echo $branch_data['branch_address']; ?></td>
                    <td><?php echo $branch_data['branch_city']; ?></td>
                    <td><?php echo $branch_data['branch_state']; ?></td>
                    <td><?php echo $branch_data['branch_pincode']; ?></td>
                    <td><?php echo $branch_data['status']; ?></td>
                    <td><a href="edit_branch_details.php?id=<?php echo $branch_data['id']; ?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"><a/></td>
                  </tr>

                <?php }; ?>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
 <?php include('../../footer.php'); ?>