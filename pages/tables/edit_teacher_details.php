<?php
 session_start();
$site_url = 'https://'.$_SERVER['HTTP_HOST'];
//$site_url = $_SERVER['PHP_SELF'];
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    };
    
include('../../header.php');

?>    
    


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage teacher
        <small>edit records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">teacher Record</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form method="POST" action="edit_teacher_details_core.php">
                <table>
  
                <?php 

                require_once "../../dbconnect.php";

                $db = new DB();

                $id = $_GET['id'];
                $teacher_query = "SELECT * FROM teacher WHERE id = '$id'";

                $select_data = $db->executeQuery($teacher_query);

                $teacher_data = mysqli_fetch_array($select_data);

                ?>


                  <tr>
                  <td><label class="m-2" for="">Allotted ID</label></td>
                  <td><input type="text" name="allotted_id" value="<?php echo $teacher_data['allotted_id']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">First Name</label></td>
                  <td><input type="text" name="fname" value="<?php echo $teacher_data['first_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Last Name</label></td>
                  <td><input type="text" name="lname" value="<?php echo $teacher_data['last_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Prefix</label></td>
                  <td><input type="text" name="prefix" value="<?php echo $teacher_data['prefix']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Email</label></td>
                  <td><input type="text" name="email" value="<?php echo $teacher_data['email']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Contact</label></td>
                  <td><input type="text" name="contact" value="<?php echo $teacher_data['contact']; ?>"></td>
                </tr>
                <input type="hidden" value="<?php echo $teacher_data['id']; ?>" name="id">
                </table>

                <div><input class="ml-15 m-3" type="submit" value="Update"></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>