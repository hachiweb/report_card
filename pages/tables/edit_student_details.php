<?php
 session_start();
$site_url = 'https://'.$_SERVER['HTTP_HOST'];
//$site_url = $_SERVER['PHP_SELF'];
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    };
    
include('../../header.php');

?>    
    


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Student
        <small>View and edit records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Student Record</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form method="POST" action="edit_details.php">
                <table>
  
                <?php 

                require_once "../../dbconnect.php";

                $db = new DB();

                $id = $_GET['id'];
                $student_query = "SELECT * FROM student WHERE id = '$id'";

                $select_data = $db->executeQuery($student_query);

                $student_data = mysqli_fetch_array($select_data);

                ?>


                  <tr>
                  <td><label class="m-2" for="">Registration No</label></td>
                  <td><input class="form-control" type="text" name="reg_no" value="<?php echo $student_data['registration_id']; ?>"></td>
                </tr>
                 <tr>
                  <td><label class="m-2" for="">Admission Year</label></td>
                  <td><input class="form-control" type="text" name="yoa" value="<?php echo $student_data['year_of_admission']; ?>" ></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">First Name</label></td>
                  <td><input class="form-control" type="text" name="fname" value="<?php echo $student_data['first_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Last Name</label></td>
                  <td><input class="form-control" type="text" name="lname" value="<?php echo $student_data['last_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">UIDAI</label></td>
                  <td><input class="form-control" type="text" name="uidai" value="<?php echo $student_data['UIDAI']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">DOB</label></td>
                  <td><input class="form-control" type="date" name="dob" value="<?php echo $student_data['dob']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Email</label></td>
                  <td><input class="form-control" type="text" name="email" value="<?php echo $student_data['email']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Address</label></td>
                  <td><input class="form-control" type="text" name="addr" value="<?php echo $student_data['address']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Mother Name</label></td>
                  <td><input class="form-control" type="text" name="mother_name" value="<?php echo $student_data['mother_name']; ?>" ></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Father Name</label></td>
                  <td><input class="form-control" type="text" name="father_name" value="<?php echo $student_data['father_name']; ?>" ></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Guardian Name</label></td>
                  <td><input class="form-control" name="guardian_name" type="text" value="<?php echo $student_data['guardian_name']; ?>" ></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Blood Group</label></td>
                  <td><input class="form-control" type="text" name="blood_group" value="<?php echo $student_data['blood_group']; ?>" ></td>
                </tr>
                <input type="hidden" class="form-control" value="<?php echo $student_data['id']; ?>" name="id">
                </table>

                <div><a href="manage_student.php" class="col-sm-2 btn btn-danger" onclick="history.go(-1);">Cancel</a><input class="col-sm-2 btn btn-success" type="submit" value="Update"></div>
               
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../../footer.php'); ?>