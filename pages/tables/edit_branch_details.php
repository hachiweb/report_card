<?php
 session_start();
$site_url = 'https://'.$_SERVER['HTTP_HOST'];
//$site_url = $_SERVER['PHP_SELF'];
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    };
    
include('../../header.php');

?>    
    


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Brunch
        <small>edit records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Brunch Record</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form method="POST" action="edit_branch_details_core.php">
                <table>
  
                <?php 

                require_once "../../dbconnect.php";

                $db = new DB();

                $id = $_GET['id'];
                $branch_query = "SELECT * FROM branch WHERE id = '$id'";

                $select_data = $db->executeQuery($branch_query);

                $branch_data = mysqli_fetch_array($select_data);

                ?>


                  <tr>
                  <td><label class="m-2" for="">Branch Code</label></td>
                  <td><input type="text" name="branch_code" value="<?php echo $branch_data['branch_code']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Branch Address</label></td>
                  <td><input type="text" name="branch_address" value="<?php echo $branch_data['branch_address']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Branch City</label></td>
                  <td><input type="text" name="branch_city" value="<?php echo $branch_data['branch_city']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Branch State</label></td>
                  <td><input type="text" name="branch_state" value="<?php echo $branch_data['branch_state']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Branch Pincode</label></td>
                  <td><input type="text" name="branch_pincode" value="<?php echo $branch_data['branch_pincode']; ?>"></td>
                </tr>
                <input type="hidden" value="<?php echo $branch_data['id']; ?>" name="id">
                </table>

                <div><input class="ml-15 m-3" type="submit" value="Update"></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include('../../footer.php'); ?>