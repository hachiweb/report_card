<?php 

 session_start();
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('../../header.php'); 
require_once '../../dbconnect.php';
      
$db = new DB();
$school_query="SELECT * FROM `school` WHERE `is_archived`=0";
$principal_query="SELECT * FROM `principal`";

$school = $db->executeQuery($school_query);
$principal = $db->executeQuery($principal_query);

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Branch Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Branch Form</li>
    </ol>
    <div id="successMessage" class="alert"></div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Branch Form</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="">
            <div class="box-body">
              <div class="form-group">
                <label for="branch_code">Branch Code</label>
                <input type="text" class="form-control" id="branch_code" name="branch_code" required>
              </div>
              <div class="form-group">
                <label for="branch_address">Address</label>
                <input type="text" class="form-control" id="branch_address" name="branch_address" required>
              </div>
              <div class="form-group">
                <label for="branch_city">City</label>
                <input type="text" class="form-control" id="branch_city" name="branch_city" required>
              </div>
              <div class="form-group">
                <label for="branch_state">State</label>
                <input type="text" class="form-control" id="branch_state" name="branch_state" required>
              </div>
              <div class="form-group">
                <label for="branch_pincode">Pincode</label>
                <input type="text" class="form-control" id="branch_pincode" name="branch_pincode" required>
              </div>
              <div class="form-group">
                <label for="branch_status">Status</label>
                <input type="radio" name="branch_status" value="1" checked="checked">Active
                <input type="radio" name="branch_status" value="0">Inactive

                <!--input type="text" class="form-control" id="branch_status" name="branch_status" required-->
              </div>
              <div class="form-group">
                <label for="school_id">School</label>
                    <select id="school_id" name="school_id" class="form-control">
                      <?php while($result = mysqli_fetch_assoc($school)){
                      echo '<option value="'.$result['id'].'">'.$result['school_name'].'</option>'; }?>
                    </select>
                </div>
              <?php /*div class="form-group">
                <label for="principal_id">Principal</label>
                    <select id="principal_id" name="principal_id" class="form-control">
                      <?php while($result = mysqli_fetch_assoc($principal)){
                      echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
                    </select>
              </div */?>
              
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <input type="submit" id="submit_btn" class="btn btn_custom"/>
            </div>                
          </form>
        </div><!-- /.box -->

      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function(){
        $("form").submit(function(e){
          e.preventDefault();
          $("successMessage").removeClass('alert-danger');
          var branch_code = $('#branch_code').val();
          var branch_address = $('#branch_address').val();
          var branch_city = $('#branch_city').val();
          var branch_state = $('#branch_state').val();
          var branch_pincode = $('#branch_pincode').val();
          var branch_status = $('#branch_status').val();
          var school_id = $('#school_id').val();
    //      var principal_id = $('#principal_id').val();
          //alert(branch_code);
          // Call ajax for pass data to other place
          $.ajax({
              type: 'POST',
              url: 'process_branch_submission.php',
              data: {
                branch_code:branch_code,
                branch_address:branch_address,
                branch_city:branch_city,
                branch_state:branch_state,
                branch_pincode:branch_pincode,
                branch_status:branch_status,
                school_id:school_id
      //          principal_id:principal_id
              },
              success: function (data) {
                $("#successMessage").addClass('alert-success');
                $("#successMessage").html(data);
                alert(data);
              },  
              
          });
      });
    });
</script>
<?php include('../../footer.php'); ?>