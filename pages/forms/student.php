<?php
 session_start();
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
        $branch_id = $_SESSION["branch_id"];
    }
  include('../../header.php');
  require_once '../../dbconnect.php';
//   $db           = new DB();

/*$standard_sql = "SELECT * FROM `standard`";
$section_sql  = "SELECT * FROM `section`";

$standard_raw = $db->executeQuery($standard_sql);
$section_raw  = $db->executeQuery($section_sql);*/

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Student Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
    <div class="alert" id="message"></div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->

        <?php /* ?>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Quick Example</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form">
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input type="file" id="exampleInputFile">

                <p class="help-block">Example block-level help text here.</p>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Check me out
                </label>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>                
          </form>
        </div><!-- /.box -->
        <?php */ ?>

        

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Student Form</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" id="studentRegistration">
            <div id="schoolFormContainer" class="box-body">
                
                <div class="form-group col-md-4"><label for="">Registration No</label><input name = "registration_no" type="text"  class="form-control" /></div>

		<!-- <div class="form-group col-md-4"><label for="">Roll No</label><input name = "roll_no" type="text"  class="form-control" /></div>  -->

		<div class="form-group col-md-4"><label for="">UIDAI</label><input name = "uidai" type="text" id="uidai" class="form-control" maxlength="14" required/></div>
            		<div class="form-group col-md-4"><label for="">Year of Admission</label><select name="yoa" class="form-control">
            <?php 
               for($i = date('Y')-12 ; $i <= date('Y'); $i++){
                  echo "<option value=".$i.">$i</option>";
               }
            ?>
            </select></div>

		<div class="form-group col-md-4"><label for="">First Name</label><input name = "first_name" type="text"  id="first_name" class="form-control" required/></div>

		<div class="form-group col-md-4"><label for="">Last Name</label><input name = "last_name" type="text" id="last_name" class="form-control" required/></div>

		<div class="form-group col-md-4"><label for="">Picture</label><input type="file" name="student_picture" accept="image/*" required/>
</div>

		<?php /*	<div class="form-group col-md-6">
									<label for="standard_id">Standard</label>
									<select name="standard_id" id="standard_id" class="form-control">
										<?php while($result = mysqli_fetch_assoc($standard_raw)){echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									</select>
								
			</div>
			<div class="form-group col-md-6">
				<label for="st_section_id">Section</label>
				<select name="section_id" id="section_id" class="form-control">
				<?php while($result = mysqli_fetch_assoc($section_raw)){echo '
				<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
			    </select>
			</div> */ ?>

		<div class="form-group col-md-6"><label for="">DOB</label><input name = "dob" type="date"  class="form-control" required/></div>
		<div class="form-group col-md-6"><label for="">Blood Group</label><input name = "blood_group" type="text" id="blood_group"  class="form-control" required/></div>

		<div class="form-group col-md-4"><label for="">Mother Name</label><input name = "mother_name" type="text" id="mother_name" class="form-control" required/></div>

		<div class="form-group col-md-4"><label for="">Father Name</label><input name = "father_name" type="text"  id="father_name" class="form-control" required/></div>

		<div class="form-group col-md-4"><label for="">Guardian Name</label><input name = "guardian_name" type="text" id="guardian_name"  class="form-control"/></div>

		<?php /*<div class="form-group col-md-4"><label for="">House No</label><input name = "house_no" type="text" id="house_no" class="form-control" required/></div>*/?>

		<div class="form-group col-md-4"><label for="">Address</label><input name = "address" type="text" id="address" class="form-control" required/></div>

		<div class="form-group col-md-4"><label for="">Contact No</label><input name = "contact" type="text" id="contact" class="form-control" required/></div>

		<div class="form-group col-md-4"><label for="">Email </label><input name = "email" type="email" id="email"  class="form-control" required/></div>

		<!--div><div style="margin-bottom:5px;">&nbsp;</div><button type="button" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove-sign"></i></button></div-->

		 <hr />

            </div>
            <!--div style="padding: 10px;">
              <div><label>To add fiels click on Add Button</label></div>
              <button id="achoolBtnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls">
                <i class="glyphicon glyphicon-plus-sign"></i>
                &nbsp; Add&nbsp;
              </button>
            </div-->
             <div class="box-footer">
              <input type="submit" id="submit_btn" class="btn btn_custom"/>
            </div> 
          </form>
        </div><!-- /.box -->

      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function(){
      jQuery('#contact').keyup(function () {
          this.value = this.value.replace(/[^0-9\.]/g,'');
      });
      jQuery("#contact").keypress(function (e) {
          var length = jQuery(this).val().length;
          if (length > 9) {
              return false;
          }
      })
      jQuery("#uidai").keypress(function (e) {
          var length = jQuery(this).val().length;
          if (length > 14) {
              return false;
          }
      })
      jQuery('#uidai').keyup(function () {
          //this.value = this.value.replace(/[^0-9 \.]/g,'').replace(/(.{4})/g, '$1 ').trim();;

      });
      $('#uidai').on('input', function (e) {

          e.target.value = e.target.value.replace(/\s+/g, '').substring(0, 12).replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
      });

    $("form").submit(function(e){
      e.preventDefault();
      $('#message').attr('style','');
      var contact=$('#contact').val();
      var uidai=$('#uidai').val();
      var error=0;
      if(contact.length!=10) {
          $("#message").addClass('alert-danger');
          $("#message").html("Mobile number is invalid");
          error=1;
      }
      if(uidai.length!=14)
      {
          $("#message").addClass('alert-danger');
          $("#message").html("UIDAI is invalid");
          error=1;
      }
      if(error==0)
      {
          var formData = new FormData(this);
          $("message").removeClass('alert-danger');
          $.ajax({
              type: 'POST',
              url: 'process_student_submission.php',
              data: formData,
              dataType:'json',
              success: function (data) {
                  $('#message').attr('style','');
                  if(data.flag==1)
                  {
                      $("#message").addClass('alert-success');
                      $("#message").html(data.msg);
                      alert(data.msg);
                      document.getElementById("studentRegistration").reset();

                  }
                  else{
                      $("#message").addClass('alert-danger');
                      $("#message").html(data.msg);
                      alert(data.msg);
                  }
                  $('#message').fadeIn('slow').delay(3000).hide(0);
              },
              cache: false,
              contentType: false,
              processData: false
          });
      }
        $('#message').fadeIn('slow').delay(3000).hide(0);
    });
  });
</script>
<?php include('../../footer.php'); ?>