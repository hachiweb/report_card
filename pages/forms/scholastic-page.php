<?php 
session_start();
$_SESSION['section_id'] = $_POST['section_id'];
$_SESSION['standard_id'] = $_POST['standard_id'];
$_SESSION['periodic_id'] = $_POST['periodic_id'];
$_SESSION['subject_id'] = $_POST['subject_id'];
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
        $branch_id = $_SESSION["branch_id"];
    }

include('../../header.php');
require_once '../../dbconnect.php';
$db           = new DB();
$teacher_sql  = "SELECT * FROM `teacher`";
$standard_sql = "SELECT * FROM `standard`";
$section_sql  = "SELECT * FROM `section`";
$exam_sql  = "SELECT * FROM `exam_meta` WHERE `branch_id`='$branch_id'";
$class_teacher = "SELECT * FROM `class-teacher` WHERE `standard_id`=1 AND `section_id` = 1";
$result = $db->executeQuery($class_teacher);
$value = mysqli_fetch_assoc($result);
$subject_sql  = "SELECT name, id FROM subject WHERE `branch_id`='$branch_id' GROUP BY name";
$stu_strength = "SELECT COUNT(`id`) FROM `student` WHERE `standard_id`=1 AND `section_id`=2";
$teacher_raw  = $db->executeQuery($teacher_sql);
$standard_raw = $db->executeQuery($standard_sql);
$section_raw  = $db->executeQuery($section_sql);
$subject_raw  = $db->executeQuery($subject_sql);
$exam_raw  = $db->executeQuery($exam_sql);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Scholastic Details
      <small>preview of class details</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Tables</a></li>
      <li class="active">Scholastic Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
            <?php /* ?>
            <div class="box-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
            <?php */ ?>
          </div>
          <!-- /.box-header -->

          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">

                  <th></th>
                  <th>Standard</th>
                  <th><select name="standard_id" id="standard_id" class="form-control">        <option value="select" >Select</option>
										<?php while($result = mysqli_fetch_assoc($standard_raw)){echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									</select></th>
                  <th>Section</th>
                  <th><select name="section_id" id="section_id" class="form-control">        
                  <option value="select" >Select                   </option>										
                  <?php while($result = mysqli_fetch_assoc($section_raw))
                  { echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									    </select></th>
                      <th>Exam Type</th>
                  <th><select name="periodic_id" id="periodic_id" class="form-control">        
                  <option value="select" >Select  </option>										
                  <?php while($result = mysqli_fetch_assoc($exam_raw)){echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									    </select></th>
                      <th>Subject</th>
                  <th><select name="subject_id" id="subject_id" class="form-control">        
                  <option value="select" >Select  </option>										
                  <?php while($result = mysqli_fetch_assoc($subject_raw)){echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									    </select></th>
                  
                  <th>Academic Session</th>
                  <th>2018-19</th>
                </tr>
            </table>
          </div><!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div><!-- row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function(){
         $("#section_id, #standard_id, #periodic_id, #subject_id").change(function(e){
            if($("#section_id").val()=="select"){
                // alert('Please select a valid section');
            }
            else if($("#standard_id").val()=="select"){
                // alert('Please select a valid standard');
            }
            else if($("#periodic_id").val()=="select"){
                // alert('Please select a valid standard');
            }
            else if($("#subject_id").val()=="select"){
                // alert('Please select a valid standard');
            }
             else {
                var section_id = $("#section_id").val();
                var periodic_id = $("#periodic_id").val();
                var standard_id = $("#standard_id").val();
                var subject_id = $("#subject_id").val();
                
                 $.ajax({
                  type: 'POST',
                  url: 'scholastic-page.php',
                  data: {
                        section_id:section_id,
                        standard_id:standard_id,
                        periodic_id:periodic_id,
                        subject_id:subject_id

                      }, 
                  success: function (data) {
                      window.location.href = 'evaluator_table.php';
                    // $("#successMessage").addClass('alert-success');
                    // $("#successMessage").html(data);
                    // $(".toggle-student-table").removeClass('toggle-student-table');
                    // $(".toggle-basic-table").removeClass('toggle-basic-table');
                    //  $(".toggle-subject-table").removeClass('toggle-subject-table');
                    
                    // alert(data);
                  },  
              
               });
               
            
            }
        });
    });
 </script>       
<?php include('../../footer.php'); ?>