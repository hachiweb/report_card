<?php
use PHPMailer\PHPMailer\PHPMailer;
require '../../vendor/autoload.php';
require_once '../../dbconnect.php';
    session_start();
	$registration_id 	=   $_POST['registration_no'];
	// $roll_no 	=   $_POST['roll_no'];
	$UIDAI 	=   (int)trim(preg_replace('/\s+/','', $_POST['uidai']));
	$first_name =   	$_POST['first_name'];
	$last_name 	=   $_POST['last_name'];
	$student_picture 	=   $school_logo = basename($_FILES['student_picture']['name']);
	//$standard_id 	=   $_POST['standard_id'];
	//$section_id 	=   $_POST['section_id'];
	$dob 	=   $_POST['dob'];
	$date1 	= 	new DateTime($dob);
	$curr_date = date('Y-m-d');
	$curr_date = new DateTime($curr_date);
	$diff = $curr_date->diff($date1)->format("%a");
	if($diff<=1277){
		echo "Student age must at least 3 years and 6 months";
		exit();
	}
	$yoa 	=   $_POST['academic_year'];
	$blood_group 	=   $_POST['blood_group'];
	$mother_name    =    	$_POST['mother_name'];
	$father_name 	=   $_POST['father_name'];
	$guardian_name 	=   $_POST['guardian_name'];
	//$house_no 	=   $_POST['house_no'];
	$address 	=   $_POST['address'];
	$contact_no 	=   (int)$_POST['contact'];
	$email    =   $_POST['email'];
	$branch_id = $_SESSION['branch_id'];

    // $pw  = substr(str_shuffle($permitted_chars), 0, 10);
   	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 10; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    // $pw  = substr(str_shuffle($permitted_chars), 0, 10);
    $pw = $randomString;
	
	$hashedpw = md5($pw);
	$db = new DB();
	
	$last_id = "SELECT id FROM `student` ORDER BY id DESC";
    $last_id = $db->executeQuery($last_id);
    $last_id = mysqli_fetch_assoc($last_id);
    $current_id = (int)$last_id['id']+1;
    $dir = dirname(__FILE__).'/assets/student/'.$current_id.'/picture';
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true);
    }
	$picture = $dir.'/'.$student_picture;
	$virtualPic = 'https://www.myschoolreportcards.com/'.'pages/forms/assets/student/'.$current_id.'/picture/'.$student_picture;
	$data=array();
    if (move_uploaded_file($_FILES['student_picture']['tmp_name'], $picture)) {
        $create_student_query = "INSERT INTO `users`(`username`,`alias`,`password`,`role`,`branch_id`) VALUES ('$registration_id','$first_name','$hashedpw','student','$branch_id')";
		$createStudent = $db->executeQuery($create_student_query);
		$sql="INSERT INTO `student`(`registration_id`,`UIDAI`,`year_of_admission`,`first_name`,`last_name`,`picture`,`dob`,`blood_group`,`mother_name`,`father_name`,`guardian_name`,`address`,`contact_no`,`email`,`branch_id`) VALUES ('$registration_id','$UIDAI','$yoa','$first_name','$last_name','$virtualPic','$dob','$blood_group','$mother_name','$father_name','$guardian_name','$address','$contact_no','$email','$branch_id')";
    	$result= $db->executeQuery($sql);
    
 
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		//Set who the message is to be sent from
	
		$mail->setFrom('su@myschoolreportcards.com', 'Digital Report Card',0);
		//Set an alternative reply-to address
		// $mail->addReplyTo('hr@hreportcard.lan', 'HR');
		//Set who the message is to be sent to
		$mail->addAddress($teacher_email);
		//Set the subject line
		$mail->Subject = 'MySchool Login credentials';
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		// $mail->msgHTML(file_get_contents('contents.html'), __DIR__);
		//Replace the plain text body with one created manually
		$mail->Body = "Username: {$teacher_email} </br> Password: {$pw}";
		// $mail->AltBody = 'This is a plain-text message body';
		//Attach an image file
		// $mail->addAttachment('images/phpmailer_mini.png');
		//send the message, check for errors
		if (!$mail->send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			// echo "Message sent!";
		}
	
		
		if ( isset($result) && isset($createStudent) && !empty($result) && !empty($createStudent)) {
			$data['flag']=1;
            $data['msg'] = "Student has successfully been registered."."<br>"."Please save the credentials- Username: ".$registration_id.' Password: '.$pw;
		  } else {
            $data['flag']=0;
            $data['msg'] = "Student could not be registered ";
	  }
      } else {
        $data['flag']=0;
        $data['msg'] = "Student picture upload failed";
      }

    echo json_encode($data);
?>    