<?php 
 
 session_start();
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('../../header.php'); 
 require_once '../../dbconnect.php';
 
  $db = new DB();

  $standard_sql="SELECT * FROM `standard`";
  $section_sql="SELECT * FROM `section`";

  $standard_raw = $db->executeQuery($standard_sql);
  $section_raw = $db->executeQuery($section_sql);

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Report Card Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
    <div id="message" class="alert"></div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Report Card</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form">
            <div class="box-body">
                <div class="form-group">
                     <table class="table table-responsive table-striped table-bordered">
                        <thead>
                         <label for="student_class">Select Exam</label>
                          <tr>
                            <td>Exam</td>
                            <td><select class="form-control exam" id="exam" name="exam">
                                <option value="Continuous Assessment">Continuous Assessment</option>
                                <option value="Continuous Assessment">Half Yearly</option>
                                <option value="Continuous Assessment">Annualy</option>
                                 <?php /*
                                     while($result = mysqli_fetch_assoc($standard_raw)){
                                        echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; 
                                    }
                               */ ?>
                            </select></td>
                            
                          </tr>
                        </thead>
                    </table>
                    <table class="table table-responsive table-striped table-bordered">
                        <thead>
                         <label for="student_class">Select Class</label>
                          <tr>
                            <td>Class</td>
                            <td><select class="form-control student_class_standard" id="student_class_standard" name="student_class_standard">
                                <option value="0">Standard</option>
                                 <?php 
                                     while($result = mysqli_fetch_assoc($standard_raw)){
                                        echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; 
                                    }
                                ?>
                            </select></td>
                            <td> <select class="form-control student_class_section" id="student_class_section" name="student_class_section">
                                <option value="0">Section</option>
                                <?php 
                                     while($result = mysqli_fetch_assoc($section_raw)){
                                        echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; 
                                    }
                                ?>
                            </select></td>
                          </tr>
                        </thead>
                    </table>
                    <table class="table table-responsive table-striped table-bordered">
                        <thead>
                         <label for="student_class">Select Student</label>
                          <tr>
                            <td>Student</td>
                            <td> <select class="form-control student" id="student" name="student">
                                <option value="St1">Stu1</option>
                                <option value="St1">Stu2</option>
                                <option value="St1">Stu3</option>
                            </select></td>
                          </tr>
                        </thead>
                    </table>    
                </div>
              
            
            <div class="table table-responsive">
              <table class="table table-responsive table-striped table-bordered">
                <thead>
                  <tr>
                    <td><sup>Dropdown List</sup>/<sub>Parameter</sub></td>
                    <td>Param 1</td>
                    <td>Param 2</td>
                    <td>Param 3</td>
                    <td>Param 4</td>
                    <td>Param 5</td>
                  </tr>
                </thead>
                <tbody id="TextBoxContainer">
                </tbody>
                <!--tfoot>
                  <tr>
                    <th colspan="5">
                      <button id="btnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls">
                        <i class="glyphicon glyphicon-plus-sign"></i>
                        &nbsp; Add&nbsp;
                      </button>
                    </th>
                  </tr>
                </tfoot-->
              </table>
            </div>
          </form>
        </div><!-- /.box -->




      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">

    $(document).ready(function(){
        $("#student_class_section").change(function(){
            var message;
            var curr = $(this);
            if($("#student_class_standard").val()=='0'){
                $("#message").removeClass('alert-success');
                $("#message").removeClass('alert-danger');
                $("#message").addClass('alert-danger');
                message = 'Please select class standard';
                $("#message").html(message);
                alert(message);
            }
            if(curr.val()=='0'){   
                $("#message").removeClass('alert-success');
                $("#message").removeClass('alert-danger');
                $("#message").addClass('alert-danger');
                message = 'Please select class section';
                $("#message").html(message);
                alert(message);
            }
        });
        $("form").submit(function(e){
          e.preventDefault();
          var formData = new FormData(this);
          $("successMessage").removeClass('alert-danger');
          $.ajax({
              type: 'POST',
              url: 'process_school_submission.php',
              data: formData,              
              success: function (data) {

                $("#successMessage").addClass('alert-success');

                $("#successMessage").html(data);

                alert(data);
              }, 
              cache: false,
              contentType: false,
              processData: false 
          });

      });

    });

</script>
<?php include('../../footer.php'); ?>