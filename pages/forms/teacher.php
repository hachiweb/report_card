<?php 
 
 session_start();
 $_SESSION['branch_id'] = $_POST['branch_id'];
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
        $branch_id = $_SESSION["branch_id"];
        require_once '../../dbconnect.php';
        $db           = new DB();
        $selected_branch_sql  = "SELECT b.id, b.branch_code,b.branch_city,s.school_name FROM branch b LEFT JOIN school s ON b.school_id = s.id WHERE b.id='$branch_id' ORDER BY b.id";

        $branch_sql = "SELECT b.id, b.branch_code,b.branch_city,s.school_name FROM branch b LEFT JOIN school s ON b.school_id = s.id ORDER BY b.id";
        
        $result1 = $db->executeQuery($selected_branch_sql);
        $value1 = mysqli_fetch_assoc($result1);
        $selected_branch = $value1['id'];
        
        $result2 = $db->executeQuery($branch_sql);
        // $value2 = mysqli_fetch_assoc($result2);
        //  print_r($selected_branch); exit();
    }
    include('../../header.php'); 
    
?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Teacher Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Teacher Form</li>
    </ol>
    <div id="successMessage" class="alert"></div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Teacher Form</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" id="teacherRegistration">
            <div class="box-body">
              <div class="form-group">
                <label for="teacher_allotted_id">Allotted Id</label>
                <input type="text" class="form-control" id="teacher_allotted_id" name="teacher_allotted_id" required>
              </div>
              <div class="row">
                 <div class="col-md-4">
                  <div class="form-group">
                    <label for="teacher_title">Title</label>
                    <select class="form-control" id="teacher_title" name="teacher_prefix"><option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Ms">Ms</option>
                    <option value="Dr">Dr</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="teacher_first_name">First Name</label>
                    <input type="text" class="form-control" id="teacher_first_name" name="teacher_first_name" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="teacher_last_name">Last Name</label>
                    <input type="text" class="form-control" id="teacher_last_name" name="teacher_last_name" required>
                  </div>
                </div>
                
              </div>
              <div class="form-group">
                    
                    <label for="branch_id">Branch</label>
                <input type="hidden" name="branch_id" id="branch_id" value="echo $_SESSION['branch_id'];?>" />

              </div>
              <div class="form-group">
                <label for="techer_email">Email</label>
                <input type="email" class="form-control" id="teacher_email" name="teacher_email" required>
              </div>
              <div class="form-group">
                <label for="teacher_contact">Contact</label>
                <input type="text" class="form-control" id="teacher_contact" name="teacher_contact" required>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn_custom">Submit</button>
            </div>                
          </form>
        </div><!-- /.box -->

      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function(){0
        jQuery('#teacher_contact').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g,'');
        });
        jQuery("#teacher_contact").keypress(function (e) {
            var length = jQuery(this).val().length;
            if (length > 9) {
                return false;
            }
        })
        $("form").submit(function(e){
          e.preventDefault();
           $("#successMessage").removeClass('alert-danger');
            teacher_allotted_id = $("#teacher_allotted_id").val();
            teacher_title = $("#teacher_title").val();
            teacher_first_name = $("#teacher_first_name").val();
            teacher_last_name = $("#teacher_last_name").val();
            teacher_email = $("#teacher_email").val();
            branch_id = $("#branch_id").val();
            teacher_contact = $("#teacher_contact").val();
            $.ajax({
                    type: 'POST',
                    url: 'process_teacher_submission.php',
                    data: {
                        teacher_allotted_id: teacher_allotted_id,
                        teacher_title: teacher_title,
                        teacher_first_name: teacher_first_name,
                        teacher_last_name: teacher_last_name,
                        teacher_email: teacher_email,
                        branch_id: branch_id,
                        teacher_contact: teacher_contact
                    },
                    success: function (data) {
                      $("#successMessage").addClass('alert-success');
                      $("#successMessage").html(data);
                      alert(data);
                     },  
                 });
            });   
    });
</script>
<?php include('../../footer.php'); ?>