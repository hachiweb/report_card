<?php include('../../header.php'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Annual Details
      <small>preview of class details</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Tables</a></li>
      <li class="active">Annual Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Annual Details Card</h3>
          </div>
          <!-- /.box-header -->
          <!-- table start -->
          <div class="custom-table annual_info_table pd-10">
            <h3 class="bg-blue pd-10">Part-1: Scholastic Area</h3>
            <table class="table table-bordered scholastic_area_table">
              <thead>                
                <tr class="text-center">
                  <th rowspan="2">S.No</th>
                  <th rowspan="2">Subjects</th>
                  <th colspan="4" class="text-center bg-blue-light">Periodic Test</th>
                  <th rowspan="2">Notebook & subject Enrichment</th>
                  <th rowspan="2">Total</th>
                  <th rowspan="2">Annual Examination</th>
                  <th rowspan="2">Grand Total</th>
                  <th rowspan="2">Grade</th>
                </tr>
                <tr>
                  <th>PT 1</th>
                  <th>PT 2</th>
                  <th>PT 3</th>
                  <th>Avg of best 2PT</th>
                </tr>
              </thead>
              <tbody>
                <tr class="text-center pd-10 bg-blue-light">
                  <td></td>
                  <td></td>
                  <td>(10)</td>
                  <td>(10)</td>
                  <td>(10)</td>
                  <td>(A=10)</td>
                  <td>(8=10)</td>
                  <td>(A+B=20)</td>
                  <td>(C=20)</td>
                  <td>(A+B=100)</td>
                  <td></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>English</td>
                  <td></td>
                  <td></td>                 
                  <td></td>                 
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Hindi / Urdu / Arabic</td>
                  <td></td>
                  <td></td>                 
                  <td></td>                 
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Mathematics</td>
                  <td></td>
                  <td></td>                 
                  <td></td>                 
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Science</td>
                  <td></td>
                  <td></td>                 
                  <td></td>                 
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Social Science</td>
                  <td></td>
                  <td></td>                 
                  <td></td>                 
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>

            <table class="table table-bordered">
              <tbody>
                <tr>
                  <tr>
                    <th rowspan="2">6</th>
                    <th>Computer Science</th>
                    <th>Practicle Exam</th>
                    <th>Annual Exam</th>
                    <th>Grand Total</th>
                    <th>Grade</th>
                  </tr>
                  <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>
                </tbody>
              </table>

              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <tr>                        
                          <th>Total Max Marks</th>
                          <th>Total Marks Obtained</th>
                          <th>Percentage</th>
                          <th>Overall Grand</th>                        
                        </tr>
                        <tr>
                          <td>600</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                      </tbody>
                    </table>                    
                  </div>
                </div>
              </div><!-- table bx -->              
            </div><!-- /.box -->

            <div class="box box-primary mt-5">
              <div class="box-header with-border">
                <div class="custom-table annual_info_table pd-10">
                  <h3 class="bg-blue pd-10">Part-2: Scholastic Area</h3>
                  <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th class="bg-blue-light">Activity</th>
                            <th class="bg-blue-light" width="150px">Grade</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>-</td>
                            <td>-</td>
                          </tr>
                          <tr>
                            <td>-</td>
                            <td>-</td>
                          </tr>
                          <tr>
                            <td>-</td>
                            <td>-</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- box -->

            <div class="box box-primary mt-5">
              <div class="box-header with-border">
                <div class="custom-table annual_info_table pd-10">
                  <h3 class="bg-blue pd-10">Part-3: Scholastic Area</h3>
                  <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                      <table class="table table-bordered text-center">
                        <thead>
                          <tr>
                            <th class="bg-blue-light">Element</th>
                            <th class="bg-blue-light">Grade</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Discipline</td>
                            <td>-</td>
                          </tr>
                        </tbody>
                      </table>

                      <h4 class="mt-5">Class Teacher Remakrs</h4>
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th class="bg-blue-light" width="150">PT 1</th>
                            <th>-</th>
                          </tr>
                          <tr>
                            <th class="bg-blue-light" width="150">PT 2</th>
                            <td>-</td>
                          </tr>
                          <tr>
                            <th class="bg-blue-light" width="150">PT 3</th>
                            <td>-</td>
                          </tr>
                          <tr>
                            <th class="bg-blue-light" width="150">Annual Examination</th>
                            <td>-</td>
                          </tr>
                        </tbody>

                        <table class="table table-bordered">
                          <tbody>
                            <tr>
                              <th class="bg-blue-light" width="150">PT 1</th>
                              <th colspan="3">-</th>
                            </tr>
                            <tr>
                              <th class="bg-blue-light" width="150">PT 2</th>
                              <td colspan="3">-</td>
                            </tr>
                            <tr>
                              <th class="bg-blue-light" width="150">PT 3</th>
                              <td colspan="3">-</td>
                            </tr>
                            <tr>
                              <th class="bg-blue-light" width="150">Annual Examination</th>
                              <td colspan="3">-</td>
                            </tr>
                            <tr>
                              <th class="bg-blue-light" width="150">Signature</th>
                              <td>Class Teacher</td>
                              <td>Principle</td>
                              <td>Parents</td>
                            </tr>
                          </tbody>
                        </table>

                        <h4 class="mt-5">Result</h4>
                        <table class="table table-bordered">
                          <tbody>
                            <tr>
                              <th class="col-sm-6" class="bg-blue-light">Result 1</th>
                              <td class="col-sm-6"></td>
                            </tr>
                            <tr>
                              <th class="bg-blue-light">Result 2</th>
                              <td>-</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- box -->

            </div><!-- col -->
          </div><!-- row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <?php include('../../footer.php'); ?>